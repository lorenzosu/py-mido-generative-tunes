""" Main script """
import sys
import signal
import time
import random

from midogen import MidoGen

def ctrl_c_handler(sig, frame):
    """ Exit if CTRL + c is pressed, i.e. SIGTERM (will wait for note ends) """
    print(f"\n\n{sig}: QUITTING - Standby as notes finish to play!\n\n")
    sys.exit(2)


def play_notes():
    """ Main function """
    mgen = MidoGen()
    print("Starting...\n")
    time.sleep(1.0)

    counter = 0
    # number of iterations to wait befor changing max waiting time
    interval_01 = 10
    # number of iterations to wait before changing transpose
    interval_02 = 70

    # initial transpose
    transpose = 0

    main_max_wait = 3000
    max_wait = main_max_wait

    signal.signal(signal.SIGINT, ctrl_c_handler)

    play = True

    while play is True:
        # next wait
        wait = random.randrange(0, max_wait) / 1000.
        
        # Generate a rest?
        should_play = random.randint(0, 100)
        if should_play <= 30:
            print("                                    "+"|- - - REST - - - ")
            time.sleep(wait)
            continue

        print(
            f"\n--------  Iteration: {counter} - {counter % interval_01} ---\n")

        # round because this must be an int
        chan, pit, vel, dur = mgen.random_note(round(max_wait * 6))
        result = mgen.make_note(
            chan, pit + transpose, vel, dur
            )
        if not result:
            continue

        mgen.random_sustain(chan)

        mgen.random_pan(chan)
        counter += 1

        if (counter % interval_01) == 0:
            #mgen.random_reverb(chan)
            max_wait = random.randint(500, main_max_wait)
            print(f"\n>>>>>> new max_wait: {max_wait}\n")

        if (counter % interval_02) == 0:
            tr_shift = random.randint(-5, 5)
            transpose += tr_shift
            print(f"\n++++++ Transposing relatively of {tr_shift}\n")

        # wait for next note
        time.sleep(wait)


if __name__ == "__main__":
    play_notes()
