Generative tunes with Python and Mido (https://mido.readthedocs.io)

Initial poliphonic prototype using multiple channels and multiple notes per channel.

Good use case fot Python's [Timer threads](https://docs.python.org/3/library/threading.html#timer-objects).

Tested on Linux with ALSA Midi and both Yoshimi and Fluidsynth.
In the yoshimi directory a sample yoshimi state file which works well with the generated tunes.

Demo video coming soon...

**How to quickly have it make noise...**
- Install [Mido](https://mido.readthedocs.io) (e.g. in a virtualenv with Pip)
- Clone the repository
- Run any synth with ALSA Midi. Example Yoshimi
- Run: `python mido_play.py`
- You will be prompted to select which ALSA Midi input to connect. Select the one of your chosen synth
- The generative tune will start to play on midi channels 1 to 6 (0 to 5)
- To stop just do CTRL + C
