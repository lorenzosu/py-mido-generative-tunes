""" Class to generate random notes and controllers """
import sys
import threading
import random

import mido

class MidoGen():
    """ Generate midi notes via Mido """
    def __init__(self):
        """ Initialize """
        # global stuff
        # Possible sustain states
        self.sustain_states = {False: "OFF", True: "ON"}
        # Valid relative intervals for random notes
        self.valid_intervals = [0, 3, 5, 7, 10]
        # List of channels to randomly pick from (start at 0!)
        self.channel_list = [0, 1, 2, 3, 4, 5]

        # Initialize dictionaries for sustain, pan and reverb in the form of
        # {chan:value} based on the available channels in channel_lists
        self.sustain_status_list = {x:0 for x in self.channel_list}
        self.pan_list = {x:64 for x in self.channel_list}
        self.reverb_list = {x:0 for x in self.channel_list}

        # list of tuples with (chan, note) keeping notes that are currently playing
        self.playing_notes = []
        self.note_threads_list = []

        # Connect the port to start with
        selected_port = self.mido_select_port()
        self.mido_connect_port(selected_port)

    def mido_connect_port(self, prt):
        """ Connext to alsa midi port and store in self.port """
        self.port = mido.open_output(prt, autoreset=True)
        if not self.port:
            print("Could not connect to a valid Alsa MIDI port, exiting.")
            print("This is a show stopper :-( exiting")
            print("Please check theres a valid MIDI port to connect to")
            sys.exit(1)
        self.port.reset()

    def mido_select_port(self):
        """ Have user select which port to connect to """
        selected = False
        while not selected:
            port_list = mido.get_output_names()
            for i, prt in enumerate(port_list):
                print(f"[{i + 1}] - {prt}")
            print()
            sel = -1
            while sel not in range(0, len(port_list) + 1):
                sel = int(input("Please select port (0 to rescan): "))
                if sel == 0:
                    selected = False
                    continue

                return port_list[sel - 1]


    def start_note(self, chan, pitch, velocity):
        """ Start a note, i.e. send a note_on message formatted with Mido.
        Add the note to the global self.playing_notes list
        """
        note_on_msg = mido.Message(
            'note_on',
            channel=chan,
            note=pitch,
            velocity=velocity
            )
        self.port.send(note_on_msg)
        self.playing_notes.append((chan, pitch))
        print("                                    "+
            f"|- o - NOTE ON:  CH{chan}, PITCH: {pitch}, VEL: {velocity}"
        )

    def stop_note(self, chan, pitch):
        """ Stop a note, i.e. send a note_off message formatted with Mido.
        Remove the stopped note from the global self.playing_notes list
        """
        note_off_msg = mido.Message('note_off', channel=chan, note=pitch)
        self.port.send(note_off_msg)
        try:
            self.playing_notes.remove((chan, pitch))
        except ValueError:
            # already stopped?
            pass
        print("                                    "+
            f"|- X - NOTE OFF: CH{chan}, PITCH: {pitch}"
            )

    def make_note(self, chan, pitch, velocity, duration):
        """ Do appropriate start_note and stop_note to generate a note of duration
        duration, unless it is already playing, i.e. in the global
        self.playing_notes list
        """
        if (chan, pitch) in self.playing_notes:
            print(f"|- ? - SKIP note {chan} {pitch} -> already playing")
            return False
        # Create a thread with the note, when done call self.stop_note
        dur_thread = threading.Timer(
            duration,
            self.stop_note,
            [chan, pitch]
        )
        self.start_note(chan, pitch, velocity)
        # Start the Timer thread which will call ths stop_note function
        dur_thread.start()
        self.note_threads_list.append(dur_thread)
        return True

    def random_channel(self):
        """ Return a random channel among the ones in self.channel_list """
        return self.channel_list[random.randrange(len(self.channel_list))]

    def random_note(self, max_duration):
        """ Generate a random note in terms of channel, pitch, velocity and
    duration. The note is only generated if it is in the self.valid_intervals list.
    This essentially means taking the modulo 12 of the note and seeing if it is
    in the list.
        """
        note_chan = self.random_channel()
        good_one = False
        while not good_one:
            note_pitch = random.randrange(25, 96)
            if note_pitch % 12 in self.valid_intervals:
                good_one = True

        note_vel = random.randrange(30, 125)
        rand_dur = random.randrange(100, max_duration) / 1000.
        return (note_chan, note_pitch, note_vel, rand_dur)


    def random_sustain(self, chan):
        """ Randomize sustain, based on current status passed as argument """
        is_on = self.sustain_status_list[chan] >= 64
        random_status = random.randint(0, 127)
        # skip if it is already on...
        if (random_status >= 64) == is_on:
            return False
        sustain_msg = mido.Message(
                'control_change',
                channel=chan,
                control=64,
                value=random_status)
        self.port.send(sustain_msg)
        print(
            "             " +
            f"|- SUSTAIN: CH{chan}: {self.sustain_status_list[chan] >= 64}"
            )
        self.sustain_status_list[chan] = random_status
        return True


    def random_reverb(self, chan):
        """ Randomize reverb (CC 91) """
        random_val = random.randint(0, 127)
        reverb_msg = mido.Message(
                    'control_change',
                    channel=chan,
                    control=91,
                    value=random_val)
        self.port.send(reverb_msg)
        self.reverb_list[chan] = reverb_msg
        print(
            "             " +
            f"|- REVERB: CH{chan}: {random_val}")
        return True


    def random_pan(self, chan):
        """ Randomize pan (CC10), actually move it left/right of 2 to 15 random """
        current_pan = self.pan_list[chan]
        random_val = random.randint(2, 15)
        random_factor = random.randint(-1, 1)
        value = (random_val * random_factor) + current_pan
        if value < 0 or value > 127:
            value = current_pan

        pan_msg = mido.Message(
                    'control_change',
                    channel=chan,
                    control=10,
                    value=value)
        self.port.send(pan_msg)
        self.pan_list[chan] = value
        print(
            "             " +
            f"|- PAN: CH{chan}: {value}"
            )
        return True
